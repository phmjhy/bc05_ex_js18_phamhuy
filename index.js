let numberArr = [];


function themSo() {
    const number = document.getElementById('txt-number').value *1;
    numberArr.push(number);
    document.getElementById(`number`).innerHTML = numberArr;
    return numberArr
}

function themSoThuc() {
    let n = document.querySelector(`#txt-num`).value * 1;
    realNumberArr.push(n);
    document.querySelector(`#txt-num`).value = ``;
    document.getElementById(`result-num`).innerHTML = realNumberArr;
    return realNumberArr;
}

// bài 1
function tinhTong() {
    let tong = 0;
    let a = numberArr;
    for (let i = 0; i < a.length ;i++) {
        if (a[i] >= 0)
        tong = tong + a[i]
    }
    document.getElementById(`result-1`).innerHTML = tong;
}

// bài 2
function demSoDuong() {
    let tongSoDuong = 0;
    let b = numberArr;
    for (let i = 0; i < b.length;i++) {
        if (b[i] > 0) {
            tongSoDuong++;
        } 
    }
    document.getElementById(`result-2`).innerHTML = tongSoDuong;
}

// bài 3
function timSoNhoNhat() {
    let minNumber = 0;
    let c = numberArr;
    for (let i = 0; i < c.length; i++) {
        if (minNumber > c[i]) {
            minNumber = c[i]
        }
    }
    document.getElementById(`result-3`).innerHTML = minNumber;
}

// bài 4 
function soDuongNhoNhat() {
    let mangSoDuong = [];
    let d = numberArr;
  
    for (let i = 0; i < d.length; i++) {
      if (d[i] > 0) {
        mangSoDuong.push(d[i]);
      }
    }
    if (mangSoDuong.length == 0) {
      document.getElementById(
        `result-4`
      ).innerHTML = `Không có số dương nhỏ nhất`;
    } else {
      let minSoDuong = mangSoDuong[0];
      for (let j = 0; j < mangSoDuong.length; j++) {
        if (minSoDuong > mangSoDuong[j]) {
          minSoDuong = mangSoDuong[j];
        }
      }
      document.getElementById(
        `result-4`
      ).innerHTML = `Số dương nhỏ nhất: ${minSoDuong}`;
    }
  }

// bài 5
function soChanCuoiCung() {
    let soChanCuoi = 0;
    let e = numberArr;
    for (let i = 0; i <e.length; i++) {
        if (e[i] % 2 == 0) {
            soChanCuoi = e[i];
        }
    }
    document.getElementById(`result-5`).innerHTML = soChanCuoi;
}

// bài 6
function doiCho() {
    let z = numberArr;
    let tmp = 0;
    let place1 = document.getElementById(`txt-place-1`).value * 1 - 1;
    let place2 = document.getElementById(`txt-place-2`).value * 1 - 1;
    tmp = z[place1];
    z[place1] = z[place2];
    z[place2] = tmp;
    document.getElementById(`result-6`).innerHTML = z;
}

// bài 7 
function sapXepTangDan() {
    let f = numberArr;
    for (let i =0; i < f.length - 1; i++ ) {
        for (let j = i + 1; j < f.length; j++) {
            if (f[j] < f[i]) {
                let t = f[i];
                f[i] = f[j];
                f[j] = t;
            }
        }
    }
    document.getElementById(`result-7`).innerHTML = f;
}

// bài 8 
let g = numberArr;
function timSoNguyenTo() {
    document.getElementById(`result-8`).innerHTML = `Bài này khó quá! <br> (◕︵◕)`;
}

// bài 9 
let realNumberArr = [];

function demSoNguyen() {
    let h = realNumberArr;
    let countSoNguyen = 0;
    for (let i = 0; i < h.length; i++) {
        if (Number.isInteger(h[i])) {
            countSoNguyen++;
        }
    }
    document.getElementById(`result-9`).innerHTML = `${countSoNguyen}`
}

// bài 10
function soSanh() {
    let j = numberArr;
    let tongSoDuong = 0;
    let tongSoAm = 0;
    for (let i = 0;i < j.length; i++) {
        if (j[i] < 0) {
            tongSoAm++;
        } else {
            tongSoDuong++;
        }
    }
    if (tongSoDuong > tongSoAm) {
        document.getElementById(`result-10`).innerHTML = `Số dương > Số âm`
    } else if (tongSoDuong < tongSoAm){
        document.getElementById(`result-10`).innerHTML = `Số dương < Số âm`
    } else {
        document.getElementById(`result-10`).innerHTML = `Số dương = Số âm`
    }
}